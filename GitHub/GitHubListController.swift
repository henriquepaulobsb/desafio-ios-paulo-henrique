//
//  GitHubListController.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import UIKit
import SwiftyJSON

class GitHubListController: NSObject {
    
    var gitHubService = GitHubService()
    
    public func getListaRepositorio(number: Int, handlerJsonResult: @escaping (JSON?) -> ()) {
        gitHubService.getListaRepositorio(number: number, handlerJsonResult: { (jsonResult) -> () in
            handlerJsonResult(jsonResult)
        })
    }
    
    
    public func getPullReposiorio(loginUsuario: String, nomeRepositorio: String, handlerJsonResult: @escaping (JSON?) -> ()) {
        gitHubService.getPullRepositorioService(loginUsuario: loginUsuario, nomeRepositorio: nomeRepositorio, handlerJsonResult: { (jsonResult) -> () in
            handlerJsonResult(jsonResult)
        })
    }
  }
