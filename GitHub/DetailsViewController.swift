//
//  DetailsViewController.swift
//  GitHub
//
//  Created by Paulo Silva on 21/01/17.
//  Copyright © 2016 Paulo Silva. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class DetailsViewController: UITableViewController {

    var myReponse : JSON = nil
    var repositorioDetals : [RepositorioDetail] = []
    
    @IBOutlet weak var openedLabel: UILabel!
    @IBOutlet weak var closedLabel: UILabel!
    
    @IBOutlet weak var descricao: UILabel!
    var barButtonBack : UIBarButtonItem!
    var buttonBack : UIButton!
    
    var itemSelecionado : Items?
    var gitHubListController = GitHubListController()
    var listRepositorio : [RepositorioDetail] = []
    var progressComponent = ProgressComponent()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        initNavigationBar()
        self.title = itemSelecionado?.name!
        progressComponent.initProgressComponent()
        getListRepositorioJava(loginUsuario: (itemSelecionado?.owner?.login!)!, nomeRepositorio: (itemSelecionado?.name!)!)
        
    }
    
    func getListRepositorioJava(loginUsuario: String,nomeRepositorio: String) {
        progressComponent.showProgressWithStatus(status: "Carregando Dados")
        gitHubListController.getPullReposiorio(loginUsuario: loginUsuario,nomeRepositorio: nomeRepositorio){ (jsonResult) in
            if jsonResult != nil{
                self.myReponse = jsonResult!
                for i in 0..<self.myReponse.count{
                    let singleRepositorio = RepositorioDetail(json:self.myReponse[i])
                    self.repositorioDetals.append(singleRepositorio)
                }
//                 self.tableView.reloadData()
            }
            
            self.progressComponent.dismissProgress(mainView: self.tableView)
            self.tableView.reloadData()
        }
    }
    
    
    func carregandoDados() {
        progressComponent.showProgressWithStatus(status: "Carregando Dados")
        Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: "runEfetuarLogin", userInfo: nil, repeats: false)
    }
    
    
    func initNavigationBar() {
    
        buttonBack = UIButton(type: .system)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        buttonBack.setImage(UIImage(named: "Back"), for: UIControlState())
        buttonBack.addTarget(self, action: #selector(DetailsViewController.barButtonBackAction), for: .touchDown)
        buttonBack.sizeToFit()
        barButtonBack = UIBarButtonItem(customView: buttonBack)
        navigationItem.leftBarButtonItem = barButtonBack
        
    }
    
    func barButtonBackAction () {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsRepository", for: indexPath) as! DetailsRepositorioJavaCell
        
        let item = self.repositorioDetals[(indexPath as NSIndexPath).row]
        
        cell.tituloLabel.text = item.title
        cell.bodyLabel.text = item.body
        cell.nomeLabel.text = item.user?.login
        
        getDataFromUrl(url: URL(string: (item.user?.avatarUrl!)!)!) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                
                cell.imageAvatar.image = UIImage(data: data)!
            }
        }
        
        var numemberOpen : String?
        var numberClosed : String?
        if let numemberOpenIssus = item.milestone?.openIssues{
            numemberOpen = NSNumber(value: (numemberOpenIssus)).stringValue
        }
        if let numemberClosedIssus = item.milestone?.closedIssues{
            numberClosed = NSNumber(value: (numemberClosedIssus)).stringValue
        }
        
         openedLabel.text = numemberOpen
         closedLabel.text = numberClosed
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.repositorioDetals[(indexPath as NSIndexPath).row]
        
        let url = URL(string: item.htmlUrl!)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositorioDetals.count
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
 }
