
import Foundation
import UIKit

class Color {
    static func aquaBlue() -> UIColor {
        return UIColor(red: 96/255, green: 197/255, blue: 240/255, alpha: 1.0)
    }
    
    static func black() -> UIColor {
        return UIColor.black
    }

    static func blue() -> UIColor {
        return UIColor(red: 23/255, green: 40/255, blue: 85/255, alpha: 1.0)
    }
    
    static func blueEstilo() -> UIColor {
        return UIColor(red: 0/255, green: 68/255, blue: 116/255, alpha: 1.0)
    }
    
    static func blueMenu() -> UIColor {
        return UIColor(red: 40/255, green: 104/255, blue: 153/255, alpha: 1.0)
    }
    
    static func blueSaldo() -> UIColor {
        return UIColor(red: 40/255, green: 145/255, blue: 201/255, alpha: 1.0)
    }
    
    static func blueBB() -> UIColor {
        return UIColor(red: 54/255, green: 91/255, blue: 156/255, alpha: 1.0)
    }

    static func blueButton() -> UIColor {
        return UIColor(red: 9/255, green: 73/255, blue: 144/255, alpha: 1.0)
    }
    
    static func blueBarMenu() -> UIColor {
        return UIColor(red: 95/255, green: 114/255, blue: 175/255, alpha: 1.0)
    }

    static func blueAtivoRecebido() -> UIColor {
        return UIColor(red: 88/255, green: 118/255, blue: 178/255, alpha: 1.0)
    }
    
    static func blueNotificationBadgeNumber() -> UIColor {
        return UIColor(red: 109/255, green: 197/255, blue: 243/255, alpha: 1.0)
    }
    
    static func blueHBK() -> UIColor {
        return UIColor(red: 8/255, green: 70/255, blue: 148/255, alpha: 1.0)
    }
    
    static func blueCustodiaButtons() -> UIColor {
        return UIColor(red: 18/255, green: 106/255, blue: 192/255, alpha: 1.0)
    }
    
    static func blueBovespa() -> UIColor {
        return UIColor(red: 68/255, green: 94/255, blue: 164/255, alpha: 1.0)
    }
    
    static func blueSaldos() -> UIColor {
        return UIColor(red: 40/255, green: 145/255, blue: 201/255, alpha: 1.0)
    }
    
    static func blueOrdens() -> UIColor {
        return UIColor(red: 0/255, green: 106/255, blue: 192/255, alpha: 1.0)
    }
    
    static func blueCarteiras() -> UIColor {
        return UIColor(red: 10/255, green: 79/255, blue: 169/255, alpha: 1.0)
    }
    
    static func blueSelection() -> UIColor {
        return UIColor(red: 68/255, green: 95/255, blue: 161/255, alpha: 1.0)
    }

    static func cpfColor() -> UIColor {
        return UIColor(red: 74/255, green: 93/255, blue: 222/255, alpha: 1.0)
    }

    static func cnpjColor() -> UIColor {
        return UIColor(red: 52/255, green: 174/255, blue: 223/255, alpha: 1.0)
    }
    
    static func clearColor() -> UIColor {
        return UIColor.clear
    }
    
    static func darkBlue() -> UIColor {
        return UIColor(red: 2/255, green: 37/255, blue: 85/255, alpha: 1.0)
    }
    
    static func darkGray() -> UIColor {
        return UIColor.darkGray
    }
    
    static func darkGrayAlertMessage() -> UIColor {
        return UIColor(red: 108/255, green: 64/255, blue: 65/255, alpha: 1.0)
    }
    
    static func darkGrayButtons() -> UIColor {
        return UIColor(red: 138/255, green: 140/255, blue: 144/255, alpha: 1.0)
    }
    
    static func grayTableHeader() -> UIColor {
        return UIColor(red: 223/255, green: 223/255, blue: 223/255, alpha: 1.0)
    }
    
    static func darkGrayMenu() -> UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
    }
    
    static func darkGreenOrdens() -> UIColor {
        return UIColor(red: 79/255, green: 138/255, blue: 72/255, alpha: 1.0)
    }

    static func darkGrayLineSeparator() -> UIColor {
        return UIColor(red: 190/255, green: 188/255, blue: 187/255, alpha: 1.0)
    }
    
    static func darkRedOrdens() -> UIColor {
        return UIColor(red: 154/255, green: 34/255, blue: 36/255, alpha: 1.0)
    }
    
    static func darkYellow() -> UIColor {
        return UIColor(red: 233/255, green: 222/255, blue: 44/255, alpha: 1.0)
    }
    
    static func gray() -> UIColor {
        return UIColor.gray
    }
    
    static func grayBarOrdens() -> UIColor {
        return UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1.0)
    }
    
    static func grayOrdens() -> UIColor {
        return UIColor(red: 138/255, green: 140/255, blue: 144/255, alpha: 1.0)
    }
    
    static func grayCell() -> UIColor {
        return UIColor(red: 119/255, green: 121/255, blue: 126/255, alpha: 1.0)
    }
    
    static func graySelection() -> UIColor {
        return UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1.0)
    }
    
    static func green() -> UIColor {
        return UIColor(red: 151/255, green: 208/255, blue: 92/255, alpha: 1.0)
    }
    
    static func greenBar() -> UIColor {
        return UIColor(red: 152/255, green: 205/255, blue: 95/255, alpha: 1.0)
    }
    
    static func greenBurn() -> UIColor {
        return UIColor(red: 101/255, green: 174/255, blue: 66/255, alpha: 1.0)
    }
    
    static func greenNegParcialmente() -> UIColor {
        return UIColor(red: 141/255, green: 192/255, blue: 68/255, alpha: 1.0)
    }
    
    static func greenAlert() -> UIColor {
        return UIColor(red: 102/255, green: 206/255, blue: 55/255, alpha: 1.0)
    }

    static func lightBlack() -> UIColor {
        return UIColor(red: 25/255, green: 26/255, blue: 30/255, alpha: 1.0)
    }
    
    static func lightBlue() -> UIColor {
        return UIColor(red: 97/255, green: 164/255, blue: 208/255, alpha: 1.0)
    }

    static func lightBlueBovespa() -> UIColor {
        return UIColor(red: 33/255, green: 143/255, blue: 202/255, alpha: 1.0)
    }
    
    static func lightGray() -> UIColor {
        return UIColor.lightGray
    }

    static func lightGrayMenu() -> UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
    }
    
    static func lightGrayMenuTab() -> UIColor {
        return UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1.0)
    }
    
    static func lighterGray() -> UIColor {
        return UIColor(red: 253/255, green: 253/255, blue: 253/255, alpha: 1.0)
    }
    
    static func lightGreenOrdens() -> UIColor {
        return UIColor(red: 104/255, green: 203/255, blue: 105/255, alpha: 1.0)
    }
    
    static func lightYellow() -> UIColor {
        return UIColor(red: 255/255, green: 255/255, blue: 155/255, alpha: 1.0)
    }
    
    static func notificacoesBlack() -> UIColor {
        return UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1.0)
    }
    
    static func notificacoesBlue() -> UIColor {
        return UIColor(red: 17/255, green: 104/255, blue: 199/255, alpha: 1.0)
    }

    static func notificacoesGray() -> UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
    }
    
    static func notificacoesGreen() -> UIColor {
        return UIColor(red: 120/255, green: 209/255, blue: 80/255, alpha: 1.0)
    }
    
    static func notificacoesRed() -> UIColor {
        return UIColor(red: 233/255, green: 103/255, blue: 99/255, alpha: 1.0)
    }
    
    static func notificacoesYellow() -> UIColor {
        return UIColor(red: 230/255, green: 231/255, blue: 104/255, alpha: 1.0)
    }
    
    static func orange() -> UIColor {
        return UIColor(red: 191/255, green: 147/255, blue: 87/255, alpha: 1.0)
    }
    
    static func red() -> UIColor {
        return UIColor(red: 175/255, green: 54/255, blue: 7/255, alpha: 1.0)
    }
    
    static func redAlertMessage() -> UIColor {
        return UIColor(red: 254/255, green: 144/255, blue: 146/255, alpha: 1.0)
    }
    
    static func redVariacaoCotacao() -> UIColor {
        return UIColor(red: 248/255, green: 48/255, blue: 39/255, alpha: 1.0)
    }
    
    static func redCancelar() -> UIColor {
        return UIColor(red: 216/255, green: 58/255, blue: 43/255, alpha: 1.0)
    }
    
    static func redSaldo() -> UIColor {
        return UIColor(red: 213/255, green: 30/255, blue: 33/255, alpha: 1.0)
    }
    
    static func redSaldoNegativo() -> UIColor {
        return UIColor(red: 213/255, green: 30/255, blue: 33/255, alpha: 1.0)
    }
    
    static func redOrdens() -> UIColor {
        return UIColor(red: 220/255, green: 52/255, blue: 52/255, alpha: 1.0)
    }
    
    static func redAlerta() -> UIColor {
        return UIColor(red: 251/255, green: 144/255, blue: 147/255, alpha: 1.0)
    }
    
    static func redAlertTitle() -> UIColor {
        return UIColor(red: 230/255, green: 78/255, blue: 84/255, alpha: 1.0)
    }
    
    static func white() -> UIColor {
        return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    }
    
    static func yellow() -> UIColor {
        return UIColor(red: 240/255, green: 220/255, blue: 0/255, alpha: 1.0)
    }
    
    static func yellowMenu() -> UIColor {
        return UIColor(red: 242/255, green: 234/255, blue: 79/255, alpha: 1.0)
    }
    
    static func yellowBurn() -> UIColor {
        return UIColor(red: 178/255, green: 170/255, blue: 52/255, alpha: 1.0)
    }
    
    static func yellowBackground() -> UIColor {
        return UIColor(red: 243/255, green: 232/255, blue: 54/255, alpha: 1.0)
    }
    
    static func yellowFavorite() -> UIColor {
        return UIColor(red: 244/255, green: 228/255, blue: 59/255, alpha: 1.0)
    }

    static func yellowBackgroundLight() -> UIColor {
        return UIColor(red: 253/255, green: 242/255, blue: 220/255, alpha: 1.0)
    }
        
    static func set(color: UIColor, alpha : CGFloat) -> UIColor {
        return color.withAlphaComponent(alpha)
    }
    
    static func setGradientInView(view:  UIView!, withMainColor mainColor: UIColor!, andSecondColor secondColor: UIColor!) {
        let gradient = CAGradientLayer()
        
        gradient.frame = CGRect(x: 0, y: 0, width: view.frame.size.width*1.5, height: view.frame.size.height*1.2)
        gradient.colors = NSArray(objects: mainColor.cgColor, secondColor.cgColor) as [AnyObject]
        
        if let sublayers = view.layer.sublayers {
            for layer in sublayers {
                if String(describing: type(of: layer)) == "CAGradientLayer" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    static func setBasicGradientInView(view:  UIView!, withMainColor mainColor: UIColor!, andSecondColor secondColor: UIColor!) {
        let gradient = CAGradientLayer()
        
        gradient.frame = CGRect(x: 0, y: 0, width: view.frame.size.width*4, height: view.frame.size.height)
        gradient.colors = NSArray(objects: mainColor.cgColor, secondColor.cgColor) as [AnyObject]
        
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    static func addBlurWithStyle(style : UIBlurEffectStyle, view : UIView) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        view.addSubview(blurEffectView)
    }
    
    static func addBlurWithStyle(view : UIView, vibrancyText: String) {
        // Blur Effect
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        view.addSubview(blurEffectView)
        
        // Vibrancy Effect
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyEffectView.frame = view.bounds
        
        // Label for vibrant text
        let vibrantLabel = UILabel()
        vibrantLabel.text = vibrancyText
        vibrantLabel.font = UIFont.systemFont(ofSize: 17.0)
        vibrantLabel.sizeToFit()
        vibrantLabel.center = view.center
        
        // Add label to the vibrancy view
        vibrancyEffectView.contentView.addSubview(vibrantLabel)
        
        // Add the vibrancy view to the blur view
        blurEffectView.contentView.addSubview(vibrancyEffectView)
    }
    
    static func colorFromHexString(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        cString = (cString as NSString).substring(from: 1)
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}
