//
//  RepositorioDetail.swift
//
//  Created by Paulo Silva on 21/01/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RepositorioDetail {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let state = "state"
    static let body = "body"
    static let links = "_links"
    static let locked = "locked"
    static let diffUrl = "diff_url"
    static let patchUrl = "patch_url"
    static let assignees = "assignees"
    static let milestone = "milestone"
    static let statusesUrl = "statuses_url"
    static let id = "id"
    static let reviewCommentUrl = "review_comment_url"
    static let base = "base"
    static let title = "title"
    static let commentsUrl = "comments_url"
    static let url = "url"
    static let issueUrl = "issue_url"
    static let user = "user"
    static let updatedAt = "updated_at"
    static let htmlUrl = "html_url"
    static let mergeCommitSha = "merge_commit_sha"
    static let number = "number"
    static let head = "head"
    static let commitsUrl = "commits_url"
    static let createdAt = "created_at"
    static let reviewCommentsUrl = "review_comments_url"
  }

  // MARK: Properties
  public var state: String?
  public var body: String?
  public var links: Links?
  public var locked: Bool? = false
  public var diffUrl: String?
  public var patchUrl: String?
  public var assignees: [Any]?
  public var milestone: Milestone?
  public var statusesUrl: String?
  public var id: Int?
  public var reviewCommentUrl: String?
  public var base: Base?
  public var title: String?
  public var commentsUrl: String?
  public var url: String?
  public var issueUrl: String?
  public var user: User?
  public var updatedAt: String?
  public var htmlUrl: String?
  public var mergeCommitSha: String?
  public var number: Int?
  public var head: Head?
  public var commitsUrl: String?
  public var createdAt: String?
  public var reviewCommentsUrl: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    state = json[SerializationKeys.state].string
    body = json[SerializationKeys.body].string
    links = Links(json: json[SerializationKeys.links])
    locked = json[SerializationKeys.locked].boolValue
    diffUrl = json[SerializationKeys.diffUrl].string
    patchUrl = json[SerializationKeys.patchUrl].string
    if let items = json[SerializationKeys.assignees].array { assignees = items.map { $0.object} }
    milestone = Milestone(json: json[SerializationKeys.milestone])
    statusesUrl = json[SerializationKeys.statusesUrl].string
    id = json[SerializationKeys.id].int
    reviewCommentUrl = json[SerializationKeys.reviewCommentUrl].string
    base = Base(json: json[SerializationKeys.base])
    title = json[SerializationKeys.title].string
    commentsUrl = json[SerializationKeys.commentsUrl].string
    url = json[SerializationKeys.url].string
    issueUrl = json[SerializationKeys.issueUrl].string
    user = User(json: json[SerializationKeys.user])
    updatedAt = json[SerializationKeys.updatedAt].string
    htmlUrl = json[SerializationKeys.htmlUrl].string
    mergeCommitSha = json[SerializationKeys.mergeCommitSha].string
    number = json[SerializationKeys.number].int
    head = Head(json: json[SerializationKeys.head])
    commitsUrl = json[SerializationKeys.commitsUrl].string
    createdAt = json[SerializationKeys.createdAt].string
    reviewCommentsUrl = json[SerializationKeys.reviewCommentsUrl].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = state { dictionary[SerializationKeys.state] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.locked] = locked
    if let value = diffUrl { dictionary[SerializationKeys.diffUrl] = value }
    if let value = patchUrl { dictionary[SerializationKeys.patchUrl] = value }
    if let value = assignees { dictionary[SerializationKeys.assignees] = value }
    if let value = milestone { dictionary[SerializationKeys.milestone] = value.dictionaryRepresentation() }
    if let value = statusesUrl { dictionary[SerializationKeys.statusesUrl] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = reviewCommentUrl { dictionary[SerializationKeys.reviewCommentUrl] = value }
    if let value = base { dictionary[SerializationKeys.base] = value.dictionaryRepresentation() }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = commentsUrl { dictionary[SerializationKeys.commentsUrl] = value }
    if let value = url { dictionary[SerializationKeys.url] = value }
    if let value = issueUrl { dictionary[SerializationKeys.issueUrl] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = htmlUrl { dictionary[SerializationKeys.htmlUrl] = value }
    if let value = mergeCommitSha { dictionary[SerializationKeys.mergeCommitSha] = value }
    if let value = number { dictionary[SerializationKeys.number] = value }
    if let value = head { dictionary[SerializationKeys.head] = value.dictionaryRepresentation() }
    if let value = commitsUrl { dictionary[SerializationKeys.commitsUrl] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = reviewCommentsUrl { dictionary[SerializationKeys.reviewCommentsUrl] = value }
    return dictionary
  }

}
