
import Foundation
import UIKit

class ProgressBarView : UIProgressView {
    var startProgress = 0
    var endProgress = 100
    var progressInterval = Double(1)
    
    var viewTrack : UIView!
    
    var colorProgress = UIColor.lightGray
    var colorTrack = UIColor.blue
    
    var cornerRadius = CGFloat(12)
    
    func setProgressBarView() {
        self.progressViewStyle = .bar
        self.tintColor = colorTrack
        self.trackTintColor = colorProgress
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadius
    }
    
    func setProgressBarContentProgress() {
        if startProgress > 0 && startProgress <= endProgress {
            self.progress = Float(startProgress)/Float(endProgress)
        }
    }
    
    func reloadContent() {
        self.progressViewStyle = .bar
//        self.tintColor = Color.clearColor()
        self.trackTintColor = colorProgress
        self.progress = 0
        self.layer.masksToBounds = true
        self.layer.cornerRadius = cornerRadius
    }
}
